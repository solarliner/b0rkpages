from setuptools import setup

setup(
    name='b0rkpages',
    version='0.0.1',
    packages=[''],
    url='',
    license='MIT',
    author='Nathan Graule',
    author_email='solarliner@gmail.com',
    description='A cuter version of the manpages, drawn by Julia Evans (@b0rk).'
)
