from argparse import ArgumentParser
from collections import namedtuple
import csv
import logging
import os, sys

from fuzzywuzzy import process
import requests

logger = logging.getLogger()

def load_data():
    data = dict()
    data_filepath = os.path.join(os.path.dirname(__file__), 'pages_data.tsv')
    logger.debug('Data file path: %s', data_filepath)
    with open(data_filepath) as tsv_file:
        tsv_in = csv.reader(tsv_file, delimiter='\t')
        for code, url in tsv_in:
            data[code] = url
    return data

def setup():
    logger.setLevel(logging.DEBUG)
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    sh.setFormatter(logging.Formatter('%(levelname)8s: %(message)s'))
    logger.addHandler(sh)

def show_image(url: str):
    r = requests.get(url)
    logger.debug(repr(r))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('command')

    args = parser.parse_args()
    
    setup()

    data = load_data()

    matches = process.extract(args.command, data.keys())
    logger.debug('Matches: %s', repr(matches))
    match = matches[0]
    if match[1] > 70:
        logger.info('Documentation for %s at %s', match, data[match])
    else:
        logger.critical("Couldn't find a drawing for %s", args.command)